<?php

use Illuminate\Support\Facades\Route;

Route::view('/', 'pages.index')->name('landing');

Route::prefix('votre-projet-web')->group(function () {
	Route::view('/', 'pages.projet.index')->name('votre-projet');
	Route::view('petit-budget', 'pages.projet.petit-budget')->name('petit-budget');
	Route::view('abonnement', 'pages.projet.abonnement')->name('abonnement');
	Route::view('sur-mesure', 'pages.projet.sur-mesure')->name('sur-mesure');
	Route::view('questionnaire-projet', 'pages.projet.questionnaire')->name('questionnaire');
});

Route::view('agence-de-communication-creusoise', 'pages.print')->name('print');
Route::redirect('print', '/agence-de-communication-creusoise');

Route::view('realisations-site-internet', 'pages.realisations')->name('realisations');

Route::view('agence-developpement-web-aubusson-creuse', 'pages.agence')->name('agence');

Route::view('contact', 'pages.contact')->name('contact');

Route::view('mentions-legales', 'pages.mentions-legales')->name('mentions');

Route::view('calendar', 'pages.calendar')->name('calendar');

Route::post('contact-agence-web-aubusson-creuse', 'PostsController@sendMail')->name('mail');

Route::redirect('immo', 'https://latoile-dev.notion.site/Dossier-d-investissement-locatif-81fb9e57d64540bfbf5a248ecabb47ec');
