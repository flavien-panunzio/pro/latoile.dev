//CAPTCHA
$(window).bind("load", function () {
	$.getScript("https://www.google.com/recaptcha/api.js?render=6LcK-sMZAAAAAF5kmyJuvdfIv77bbbsmsH6hfDVL", function (data, textStatus, jqxhr) {
		grecaptcha.ready(function () {
			grecaptcha.execute("6LcK-sMZAAAAAF5kmyJuvdfIv77bbbsmsH6hfDVL", { action: 'homepage' }).then(function (token) {
				document.getElementById('g-recaptcha-response').value = token;
			});
		});
	});
});
