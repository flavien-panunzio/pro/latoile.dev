// Hover-dir responsive --------------------------------------------------------
if (is_touch_device() && $('#da-thumbs').length) {
	$(".carre-hover div").css({ "top": "0px", "left": "0px" })
} else {
	if ($('#da-thumbs').length) {
		$.getScript(url + "/js/hover-dir.js", () => {
			$('#da-thumbs > .carre-hover').hoverdir();
		});
	}
}

function is_touch_device() {
	try {
		document.createEvent("TouchEvent");
		return true;
	} catch (e) {
		return false;
	}
}
