@extends('layout.main')
@section('title',"L'agence")
@section('description',"Votre agence web crée pour vous des expériences digitales innovantes en vous proposant des solutions web réfléchies, abouties et pertinentes. Venez découvrir votre agence web à Aubusson !")
@section('header')
<h1>LaToile.dev</h1>
<h2>Agence Web : création de sites internet</h2>
<p>Spécialisés dans la création de sites internet personnalisés, nous prenons en charge votre projet de A à Z et nous vous accompagnons tout au long de votre projet</p>
@endsection
@section('includes')
<link rel="stylesheet" href="{{ asset('css/agence.css') }}">
<script src="{{ asset('js/agence.js') }}" defer></script>
@endsection

@section('content')
<main class="agence container">
	<div class="quisommesnous">
		<h2>Qui sommes-nous ?</h2>
		<p>LaToile.dev est une agence Web située dans la magnifique ville médiévale d'Aubusson. Vous trouverez sur cette page la présentation de notre savoir-faire dans la création de sites internet personnalisés.</p>
		<h2>Des <b>valeurs</b> avant tout</h2>
		<div class="row">
			<div class="col-md-6">
				<h3>La Proximité</h3>
				<p>Ici, <b>	PAS</b> de développement web dans une contrée lointaine. Nous travaillons sur Aubusson et tenons à garder un lien de <b>proximité avec nos clients</b>.</p>
			</div>
			<div class="col-md-6">
				<h3>La Confiance</h3>
				<p>Cette proximité nous permet de lier une <b>relation de confiance avec nos clients</b>, car avant d'être notre travail, ce projet est le leur.</p>
			</div>
			<div class="col-md-6">
				<h3>La Passion</h3>
				<p>Notre générosité dans l’effort provient de notre <b>passion</b> pour ce métier. Nous suivons régulièrement les nouvelles technologies afin de proposer <b>toujours de plus belles réalisations</b>.</p>
			</div>
			<div class="col-md-6">
				<h3>La Collaboration</h3>
				<p>Coupler votre expertise métier avec notre expertise technique, c’est développer <b>un projet commun</b> intelligent et performant. Nous travaillons avec vous, pour que votre projet reste <b>unique et authentique</b>.</p>
			</div>
		</div>
	</div>
	{{-- <div>
		<h2>Nos engagements</h2>
		<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Doloremque consequatur nam illo cupiditate, velit tempore dignissimos commodi quasi eos impedit deleniti. Molestiae quis quia optio magni molestias consequuntur? Possimus, dicta.</p>
	</div> --}}
	<div>
		<h2>Pourquoi "LaToile<span class="typo">.dev</span>" ?</h2>
		<p>"LaToile" représente le lien que nous allons tisser entre vous et vos clients tandis que ".dev" signifie le développement Web qui sera nécessaire à la réalisation de votre projet. En effet, nous développons "à la main" chaque site internet afin de le rendre unique et personnalisable à l'infini.</p>
	</div>
	<div>
		<cite><i class="fa fa-quote-left mr-3 fa-lg"></i> Celui qui croît qu'un professionnel coûte cher n'a aucune idée de ce que peut lui coûter un incompétent ! <i class="fa fa-quote-right ml-3 fa-lg"></i></cite>
	</div>
	{{-- <div class="stats">
		<h2>Nos compétences :</h2>
		<p>Nous sommes spécialisés dans le développement Full-Stack afin de pouvoir répondre à toute sorte de prestations</p>
		<ul>
			<li>Développement Web (Back-End)</li>
			<li>Intégration visuelle (Front-End)</li>
			<li>Référencemence (SEO)</li>
			<li>Optimisation Web</li>
			<li>Serveur</li>
		</ul>
		<code>Stats avec annimations</code>
	</div> --}}
	<div class="wall-of-fame">
		<h2>Ils nous font confiance : </h2>
		<p>Et c’est déjà beaucoup pour nous !</p>
		<div class="row">
			<div class="col-lg-10 col-xl-8 mx-auto">
				<div class="bg-white shadow rounded">
					<div class="carousel slide" id="quote-carousel" data-ride="carousel">
						<ol class="carousel-indicators mb-0">
							<li class="active" data-target="#quote-carousel" data-slide-to="0"></li>
							<li data-target="#quote-carousel" data-slide-to="1"></li>
							{{-- <li data-target="#quote-carousel" data-slide-to="2"></li> --}}
						</ol>
						<div class="carousel-inner px-5 pb-4">
							<div class="carousel-item active">
								<div class="media row">
									<div class="col-md-2">
										<img class="rounded-circle img-thumbnail" src="{{ asset('images/kalkeo.png') }}" alt="Logo de kalkeo.com" width="75">
									</div>
									<div class="media-body ml-3 col-md-10">
										<blockquote class="blockquote border-0 p-0">
											<p class="font-italic lead"> <i class="fa fa-quote-left mr-3 fa-lg"></i>Flavien a réalisé mon site internet, il a su s’adapter et répondre au mieux à ma demande. C’est un très bon Webmaster, je le recommande. Merci encore. <i class="fa fa-quote-right ml-3 fa-lg"></i></p>
											<div class="blockquote-footer">Manon Roche de
												<cite title="Source Title"><a href="https://www.kalkeo.com/">Kalkeo</a></cite>
											</div>
										</blockquote>
									</div>
								</div>
							</div>
							<div class="carousel-item">
								<div class="media row">
									<div class="col-md-2">
										<img class="rounded-circle img-thumbnail" src="{{ asset('images/RG.png') }}" alt="Logo du Relais de la Garde" width="75">
									</div>
									<div class="media-body ml-3 col-md-10">
										<blockquote class="blockquote border-0 p-0">
											<p class="font-italic lead"> <i class="fa fa-quote-left mr-3 fa-lg"></i>Flavien fait du très bon travail ! Il a réalisé mon site du <a href="https://www.relaisdelagarde.fr/">Relais de la Garde</a> et c'est parfait ! Je recommande vivement ! <i class="fa fa-quote-right ml-3 fa-lg"></i></p>
											<div class="blockquote-footer">Phillipe Gauthier du
												<cite title="Source Title"><a href="https://www.relaisdelagarde.fr/">Relais de la Garde</a></cite>
											</div>
										</blockquote>
									</div>
								</div>
							</div>
							{{-- <div class="carousel-item">
								<div class="media row">
									<div class="col-md-2">
										<img class="rounded-circle img-thumbnail" src="{{ asset('images/lespetitesmains.png') }}" alt="Logo des petites mains du limousin" width="75">
									</div>
									<div class="media-body ml-3 col-md-10">
										<blockquote class="blockquote border-0 p-0">
											<p class="font-italic lead"> <i class="fa fa-quote-left mr-3 fa-lg"></i> Nous avons fait appel à LaToile.dev pour la refonte de notre site internet, tout c'est très bien passé, merci à vous. <i class="fa fa-quote-right ml-3 fa-lg"></i></p>
											<div class="blockquote-footer">Frédéric Guedj de
												<cite title="Source Title"><a href="https://www.lespetitesmainsdulimousin.com/">Les petites mains du Limousin</a></cite>
											</div>
										</blockquote>
									</div>
								</div>
							</div> --}}
						</div>
						<a class="carousel-control-prev width-auto" href="#quote-carousel" role="button" data-slide="prev">
							<i class="fa fa-angle-left fa-2x"></i>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next width-auto" href="#quote-carousel" role="button" data-slide="next">
							<i class="fa fa-angle-right fa-2x"></i>
							<span class="sr-only">Next</span>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection
