@extends('layout.main')
@section('title',"Agence de communication Creusoise à Aubusson")
@section('description',"Votre agence de communication en creuse vous accompagne dans tous vos projets, du design d'un logo personnalisé, à la création d'un site web tout en passant par l'impression de vos différents supports de communication.")

@section('includes')
<link rel="stylesheet" href="{{ asset('css/print.css') }}">
<script src="{{ asset('js/print.js') }}" defer></script>
@endsection

@section('header')
<div class="container">
	<h1>L'Agence de Communication Creusoise</h1>
	<h2 class="pt-5">Design et communication sur tous supports</h2>
	<img src="{{ asset('images/print/speak.svg') }}" class="speak" alt="comunication evenementiel">
	<img src="{{ asset('images/print/speak.svg') }}" class="speak reverse" alt="agence de communication en creuse">
	<p>Votre agence de communication locale vous accompagne dans votre communication : de la création des designs à l'impression.</p>
</div>
@endsection


@php
$reals=[
[asset('images/print/reals/kouillere.jpg'), "flocage d'un camion foodtruck"],
[asset('images/print/reals/sarlamaconnerie.jpg'), "flocage de vêtements personnalisés, casquette brodée, pull brodé, t-shirt floqué"],
[asset('images/print/reals/bache_toits.jpg'), "design et impression d'une bâche pour un évènement à Jarnages"],
[asset('images/print/reals/eb23-2.jpg'), "flocage de la lunette arrière d'un véhicule"],
[asset('images/print/reals/rulliere.jpg'), "realisation d'une vitrine sur mesure à aubusson"],
[asset('images/print/reals/fantine-2.jpg'), "impression de flyers pas chère en creuse"],
[asset('images/print/reals/coq-dor.jpg'), "panneaux publicitaires personnalisés pas chère"],
[asset('images/print/reals/db23.jpg'), "création de logo aubusson pas chère"],
[asset('images/print/reals/vincent.jpg'), "flocage de voiture"],
[asset('images/print/reals/lestoits.jpg'), "création de flyers personnalisés en creuse"],
[asset('images/print/reals/cactus.jpg'), "création de logo en creuse"],
[asset('images/print/reals/music-business.jpg'), "creation de logo à proximité"],
[asset('images/print/reals/dolmaci.jpg'), "realisation d'un flocage de véhicule d'un bucheron"],
[asset('images/print/reals/casse_ferrari.jpg'), "design de cartes de visites aubusson"],
[asset('images/print/reals/eb23.jpg'), "signalétique panneau vitrine en creuse"],
[asset('images/print/reals/fantine.jpg'), "design et impression de flyers"],
[asset('images/print/reals/kalkeo1.png'), "impression de prospectus en creuse"],
[asset('images/print/reals/kalkeo1.1.jpg'), "design de cartes de visites pas chère"],
[asset('images/print/reals/kalkeo1.2.jpg'), "impression cartes de visite"],
[asset('images/print/reals/lafleur.jpg'), "graphiste en creuse"],
[asset('images/print/reals/malherbe.jpg'), "imprimeur en creuse"],
[asset('images/print/reals/matron.jpg'), "designeur creusois"],
[asset('images/print/reals/natorel.png'), "creation graphique personnalisées"],
[asset('images/print/reals/natorel2.jpg'), "creation de logo personnalisé"],
[asset('images/print/reals/natorel3.jpg'), "design de packaging"],
[asset('images/print/reals/natorel4.jpg'), "design de flyers"],
[asset('images/print/reals/paul.jpg'), "impression de panneaux de chantiers"],
[asset('images/print/reals/paul2.jpg'), "flocage de camion de chantier"],
[asset('images/print/reals/paul3.jpg'), "bache de signalisation de chantier"],
[asset('images/print/reals/serge1.jpg'), "impression de flyers pour artisan"],
[asset('images/print/reals/serge2.jpg'), "impression de cartes de visite pour artisan"],
[asset('images/print/reals/vinz.png'), "carte de visite sur mesure"],
[asset('images/print/reals/velo1.jpg'), "creation de vitrine sur mesure"],
[asset('images/print/reals/velo2.jpg'), "creation de logo sur mesure"],
[asset('images/print/reals/velo3.jpg'), "creation de design sur mesure"],
];
@endphp


@section('content')
<main class="container-fluid px-2 px-md-5 print">
	<h2>Nos Services</h2>
	<div class="services">
		<div class="service shadow-lg rounded-lg" data-toggle="collapse" data-target="#collapseDesign" aria-expanded="false" aria-controls="collapseDesign">
			<img src="{{ asset('images/print/design.jpg') }}" class="img" alt="design personnalisé">
			<div class="content">
				<h3 class="shadow">Design</h3>
			</div>
		</div>
		<div class="service shadow-lg rounded-lg" data-toggle="collapse" data-target="#collapseImpression" aria-expanded="false" aria-controls="collapseImpression">
			<img src="{{ asset('images/print/impression.jpg') }}" class="img" alt="impression sur mesures">
			<div class="content">
				<h3 class="shadow">Impression</h3>
			</div>
		</div>
		<div class="service shadow-lg rounded-lg" data-toggle="collapse" data-target="#collapseSignaletique" aria-expanded="false" aria-controls="collapseSignaletique">
			<img src="{{ asset('images/print/signaletique.jpg') }}" class="img" alt="signalisation évenementielle">
			<div class="content">
				<h3 class="shadow">Signalétique</h3>
			</div>
		</div>
		<div class="service shadow-lg rounded-lg" data-toggle="collapse" data-target="#collapseFlocage" aria-expanded="false" aria-controls="collapseFlocage">
			<img src="{{ asset('images/print/flocage.webp') }}" class="img" alt="autocollant voiture en creuse">
			<div class="content">
				<h3 class="shadow">Flocage</h3>
			</div>
		</div>
	</div>
	<div class="collapses">
		<div class="collapse" id="collapseDesign">
			<div class="card">
				<div class="row no-gutters">
					<div class="col-md-4">
						<img src="{{ asset('images/print/declinaisons.webp') }}" class="img" alt="création de logo moderne">
					</div>
					<div class="col-md-8 flex-center">
						<div class="card-body flex-center">
							<h2 class="card-title">Design de Chartes Graphiques</h2>
							<p class="no-padding">
								Nous vous accompagnons dans le choix et dans la création de votre charte graphique, car c'est une étape importante et primordiale dans votre communication. En effet, le fait d'avoir une identité visuelle qui vous est propre (logo, nom d'entreprise, couleurs, typographies, etc.) ajoute une réelle plus-value à votre activité.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="collapse" id="collapseImpression">
			<div class="card">
				<div class="row no-gutters">
					<div class="col-md-4">
						<img src="{{ asset('images/print/mockup.png') }}" class="img" alt="impression pour evenement">
					</div>
					<div class="col-md-8 flex-center">
						<div class="card-body flex-center">
							<h2 class="card-title">Impression sur tous supports</h2>
							<p class="no-padding">
								Afin de communiquer localement, il vous sera rapidement indispensable d'imprimer plusieurs supports de communications :
							</p>
							<ul class="no-padding" style="list-style-position: inside;">
								<li>Cartes de visite</li>
								<li>Flyers, dépliants, affiches, etc.</li>
								<li>Vêtements personnalisés</li>
								<li>Packaging</li>
								<li>Communication évènementielle</li>
								<li>Goodies et Objets Publicitaires (stylos, mugs, portes clés, etc.)</li>
								<li>Carteries (faire-part et invitations, etc.)</li>
								<li>Papeterie (fonds de documents, calendriers, enveloppes, tampons encreurs, etc.)</li>
							</ul>
							<p class="no-padding">
								Si votre support ne se trouve pas dans cette liste, <b><a href="{{ route('contact') }}">contactez-nous</a></b> ! Nous sommes certains que nous avons ce que vous souhaitez.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="collapse" id="collapseSignaletique">
			<div class="card">
				<div class="row no-gutters">
					<div class="col-md-4">
						<img src="{{ asset('images/print/enseigne.jpg') }}" class="img" alt="vitrine de boutique design">
					</div>
					<div class="col-md-8 flex-center">
						<div class="card-body flex-center">
							<h2 class="card-title">Signalétique</h2>
							<p class="no-padding">
								La signalétique est également une grosse étape de la communication visuelle, que ce soit pour réaliser une enseigne de votre magasin ou pour imprimer des bâches à fixer à votre échafaudage, nous sommes certains d'avoir une solution à vos besoins.
							</p>
							<p class="no-padding">
								<a href="{{ route('contact') }}">Contactez-nous</a> pour échanger à propos de votre projet afin que nous trouvions le support de communication le plus adapté à votre entreprise.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="collapse" id="collapseFlocage">
			<div class="card">
				<div class="row no-gutters">
					<div class="col-md-4">
						<img src="{{ asset('images/print/vitrine.webp') }}" class="img" alt="autocollants sur vitrine">
					</div>
					<div class="col-md-8 flex-center">
						<div class="card-body flex-center">
							<h2 class="card-title">Flocage de véhicules et de vitrines</h2>
							<p class="no-padding">
								Faites de vos véhicules d’entreprise ou de votre vitrine de véritables outils de communication !
								Nous réalisons des <b>flocages d'autocollants sur des vitrines ou des véhicules</b>.
							</p>
							<p class="no-padding">
								Tout en respectant votre charte graphique, vous pouvez notament inscrire vos coordonnées et vos horaires sur votre <b>flocage personnalisé</b>.
							</p>
						</div>
					</div>
				</div>
			</div>
		</div>
		<h2>Nos Réalisations</h2>
		<div class="reals">
			@foreach ($reals as $real)
			<figure>
				<img class="img" src="{{$real[0]}}" alt="{{$real[1]}}" data-toggle="modal" data-target="#modalZoom" data-image="{{$real[0]}}">
			</figure>
			@endforeach
		</div>
	</div>
</main>
@endsection

@section('modal')
<div class="modal fade" id="modalZoom" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered modal-xl">
		<div class="modal-content">
			<div class="modal-body no-padding">
				<img id="modal-img" class="img">
			</div>
		</div>
	</div>
</div>3
@endsection
