@extends('layout.main')
@section('title',"Agenda des formations au numérique")
@section('description',"Retrouvez ici mon planing pour les formations au numérique.")
@section('index',"no-index")

@section('header')
<h1>Formations au numérique</h1>
<p>Retrouvez ici mon planing pour les formations au numérique.</p>
@endsection

@section('content')
<main class="container">
	<h2>Mon agenda partagé</h2>
	<iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=2&amp;bgcolor=%23ffffff&amp;ctz=Europe%2FParis&amp;src=cjE1aWs1YzVkdDE4dDZqZWFiYWNrZ3RkM2NAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;color=%23F4511E&amp;mode=WEEK&amp;showCalendars=0&amp;showTabs=1&amp;showTz=0&amp;showPrint=0&amp;showDate=1&amp;title=Formations%20au%20num%C3%A9riques%20Flavien%20Panunzio" style="border-width:0" width="100%" height="600" frameborder="0" scrolling="no"></iframe>
</main>
@endsection
