@extends('layout.main')
@section('title',"Nos réalisations")
@section('description',"Votre agence Web vous présente toutes ses plus belles créations de site internet, site e-commerce et site vitrine.")

@section('background', asset('images/background.jpg'))
@section('header')
	<h1>Nos réalisations</h1>
	<h2>Wall-of-Fame</h2>
	<p>Nous mettons tout notre savoir-faire en œuvre afin de rendre votre projet unique</p>
@endsection

@section('includes')
<link rel="stylesheet" href="{{ asset('css/realisations.css') }}">
<script src="{{ asset('js/realisations.js') }}" defer></script>
@endsection

@php
$sites=[
["CDAD", "Conception et développement d'un site Web vitrine pour une institution", "https://cdad-creuse.justice.fr/", asset('images/realisations/CDAD.jpg')],
["FNGIC", "Création d'une plateforme complète d'adhésion et de gestion des adhérents d'une fédération", "https://fngic.fr/", asset('images/realisations/fngic.jpg')],
["SARL La Maçonnerie", "Site vitrine présentant les différents services d'un maçon en Saône et Loire", "https://sarllamaconnerie.fr/fr", asset('images/realisations/paul.jpg')],
["Exploitation Bois 23", "Création d'un site web et d'une identité graphique complète", "https://exploitationbois23.fr/", asset('images/realisations/eb23.jpg')],
["QrDesigner", "Création d'un générateur de QR Codes avec paiement en ligne", "https://qrdesigner.app/fr", asset('images/realisations/qrdesigner.jpg')],
["Natorel", "Site internet de produits Bio et d'hébergements insolites", "https://natorel.fr/", asset('images/realisations/natorel.jpg')],
["Rezilli", "Back-end d'une plateforme de resiliation de courrier en ligne", "https://rezilli.com/", asset('images/realisations/rezilli.jpg')],
["Art de la Pierre", "Artisan Maçon - Tailleur de Pierres", "https://artdelapierre.net/", asset('images/realisations/artdelapierre.jpg')],
["Matron Steven", "Peintre en bâtiment sur Bourges", "https://macon-creuse.fr/", asset('images/realisations/loustalot.jpg')],
["Bousquet Daniel", "Ramonage et maçonnerie en Creuse", "https://www.bousquet-daniel.fr/", asset('images/realisations/ramoneur.jpg')],
["Les Petites Mains du Limousin", "Friperie de vente au détail et en gros à Aubusson (23)", "https://www.lespetitesmainsdulimousin.com", asset('images/realisations/lespetitesmains.jpg')],
["Site de vente en ligne", "Réalisation de maquettes d'un site de vente en ligne de vêtements",null, asset('images/realisations/mockup.jpg')],
["Kalkeo", "Canyoning et Spéléologie en région Grenobloise", "https://www.kalkeo.com", asset('images/realisations/kalkeo.jpg')],
["Flavien Panunzio", "Portfolio étudiant", "https://flavien-panunzio.tk", asset('images/realisations/portfolio.jpg')],
["Le Relais de la Garde","Chambre d'hôte et élevage de chevaux","http://relaisdelagarde.fr", asset('images/realisations/relaisdelagarde.jpg')]
];
@endphp


@section('content')
<main class="realisations">
	<div>
		<h2>Voici un aperçu de nos différentes <b>créations Web</b></h2>
		<div class="row da-thumbs" id="da-thumbs">
			@foreach ($sites as $site)
			<div class="col-xl-4 col-lg-6 carre-hover">
				<img class="img" src="{{ $site[3] }}" alt="{{ $site[0].' | '.$site[1] }}">
				<div class="flex-center">
					<h3>{{ $site[0] }}</h3>
					<p>{{ $site[1] }}</p>
					@if ($site[2])
						<a href="{{ $site[2] }}" class="btn btn-primary">Voir le site</a>
					@else
						<button class="btn btn-primary" disabled>Non disponible</button>
					@endif
				</div>
			</div>
			@endforeach
		</div>
	</div>
</main>
@endsection
