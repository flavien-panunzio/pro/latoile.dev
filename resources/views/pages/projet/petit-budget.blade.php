@extends('layout.main')
@section('title',"L'offre \"Petit Budget\"")
@section('description',"Retrouvez l'offre \"Petit-Budget\", un site one-page présentant simplement votre activité. Une page simple reprenant tous les indispensables d'un site web.")
@section('header')
<h1>"Le Petit budget"</h1>
<h2>One-Page</h2>
<p>Une page simple reprenant tous les indispensables d'un site web</p>
@endsection
@section('includes')
<link rel="stylesheet" href="{{ asset('css/votre-projet.css') }}">
@endsection

@section('content')
<main class="petit-budget row">
	<div class="container">
		<h2>UN SITE SIMPLE, <b>MAIS EFFICACE</b></h2>
		<p>Cette offre "One-page" est l'offre la plus simple que nous proposons pour des personnes lançant leur activité. Le site est constitué d'une seule page, ce qui facilite la navigation de l'utilisateur.</p>
	</div>
	<div class="col-md-6">
		<h2>Les avantages</h2>
		<table class="table table-hover">
			<tbody>
				<tr>
					<td>Une navigation rapide et intuitive sur mobile</td>
				</tr>
				<tr>
					<td>Un référencement plus simple</td>
				</tr>
				<tr>
					<td>Clarté des informations (tout se trouve au même endroit)</td>
				</tr>
				<tr>
					<td>Faible coût de mise en place</td>
				</tr>
				<tr>
					<td>Une visualisation simplifiée des statistiques</td>
				</tr>
			</tbody>
		</table>
	</div>
	<div class="col-md-6 tarif flex-center">
		<h2>Le Tarif</h2>
		<span class="typo">à partir de 490€</span>
		<p>Serveur et nom de domaines compris la première année</p>
	</div>
	<div class="col-12 container exemple">
		<h2>Un exemple</h2>
		<div class="container">
			<img src="{{ asset('images/one-page.jpg') }}" alt="Un exemple de site 'one-page' que nous avons réalisé">
		</div>
	</div>
</main>
@endsection
