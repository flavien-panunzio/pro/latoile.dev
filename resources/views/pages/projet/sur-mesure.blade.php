@extends('layout.main')
@section('title',"Un site \"Sur-mesure\"")
@section('description',"Vous souhaitez un site internet sur-mesure ? Parlez-nous de votre projet afin que nous vous réalisions un devis personnalisé gratuitement.")
@section('header')
<h1>"Le sur-mesure"</h1>
<h2>All-you-want</h2>
<p>Chaque projet est unique et mérite un site qui lui ressemble</p>
@endsection
@section('includes')
<link rel="stylesheet" href="{{ asset('css/votre-projet.css') }}">
@endsection

@section('content')
<main class="sur-mesure">
	<div class="container">
		<h2>UN SITE WEB <b>À LA CARTE</b></h2>
		<p>Si aucune de nos offres correspond à vos besoins, nous vous proposons un site ainsi qu'un devis entièrement personnalisé. Afin de connaître au mieux vos besoins, nous vous invitons à remplir un questionnaire pour nous parler votre projet et que nous puissions faire un devis personnalisé.</p>
		<a href="{{ route('questionnaire') }}" class="btn btn-danger">Établir un devis gratuit</a>
	</div>
</main>
@endsection
