@extends('layout.main')
@section('title',"Questionnaire d'analyse de projet")
@section('index',"no-index")
@section('description',"Merci de remplir ce questionnaire afin de connaître au mieux votre projet")
@section('header')
<h1>Analyse de projet</h1>
<h2>Parlez-nous de vous</h2>
<p>Afin de connaître au mieux votre projet nous vous invitons à remplir ce petit questionnaire</p>
@endsection
@section('includes')
<link rel="stylesheet" href="{{ asset('css/votre-projet.css') }}">
@endsection

@section('content')
<main class="container questionnaire">
	<h2>Le Questionnaire</h2>
	<iframe src="https://docs.google.com/forms/d/e/1FAIpQLScZ1SLfDTPoVDUZR2na0i7D0m-uW77jZyIKTvM609Pb59VvUg/viewform?embedded=true" frameborder="0">Chargement…</iframe>
</main>
@endsection
