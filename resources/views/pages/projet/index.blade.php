@extends('layout.main')
@section('title',"Votre Projet")
@section('description',"Vous avez un projet de création de site internet ? LaToile.dev est votre agence spécialisée en création et refonte de site web à Aubusson. Trouvez l'offre qui vous correspond parmi celles que nous vous proposons.")
@section('header')
<h1>Votre projet</h1>
<h2>Un site internet qui vous ressemble</h2>
<p>Chaque site internet doit être unique et est associé à une offre qui lui correspond</p>
@endsection
@section('includes')
<link rel="stylesheet" href="{{ asset('css/votre-projet.css') }}">
@endsection


@section('content')
<main class="votre-projet">
	<div class="choix-offre" id="choix-offre">
		<h2>Une offre pour chaque besoin</h2>
		<div class="row">
			<div class="col-md-4 flex-center div-link" data-link="{{ route('petit-budget') }}">
				<i class="fas fa-3x fa-scroll"></i>
				<h3>L'offre <br><b>"Petit budget"</b></h3>
				<p>Un site <i>"One-Page"</i> présente simplement votre activité en une seule page.</p>
				<a href="{{ route('petit-budget') }}" class="btn btn-primary">En savoir plus</a>
			</div>
			<div class="col-md-4 flex-center div-link" data-link="{{ route('abonnement') }}">
				<i class="fas fa-3x fa-cubes"></i>
				<h3>Un abonnement <br><b>"Tout-compris"</b></h3>
				<p>Un site sous la forme d'abonnement mensuel tout compris (serveurs, nom de domaine, maintenance...)</p>
				<a href="{{ route('abonnement') }}" class="btn btn-primary">En savoir plus</a>
			</div>
			<div class="col-md-4 flex-center div-link" data-link="{{ route('sur-mesure') }}">
				<i class="fas fa-3x fa-drafting-compass"></i>
				<h3>Un site <br><b>"Sur-mesure"</b></h3>
				<p>Un site personnalisé et unique réalisé pour votre projet répondant exactement à vos besoins</p>
				<a href="{{ route('sur-mesure') }}" class="btn btn-primary">En savoir plus</a>
			</div>
		</div>
	</div>
	<div class="deroulement">
		<h2>Comment se déroule la création d'un site web ?</h2>
		<div class="grid">
			<div class="flex-center">
				<h3><span class="typo">1.</span>Première rencontre</h3>
				<p>Il est important pour nous que le client nous parle de son projet et nous présente ses enjeux afin de réaliser un site adapté à ses besoins.</p>
			</div>
			<div class="flex-center">
				<h3><span class="typo">2.</span>Le Cahier des charges</h3>
				<p>Un cahier des charges est réalisé et validé par le client afin d'être sûr d'avoir les mêmes idées pour la réalisation de ce projet.</p>
			</div>
			<div class="flex-center">
				<h3><span class="typo">3.</span>Développement du site</h3>
				<p>Une fois le projet bien défini, il est temps de passer au développement du site. Tout au long de cette étape, il y a régulièrement des échanges avec le client afin de prendre la bonne direction.</p>
			</div>
			<div class="flex-center">
				<h3><span class="typo">4.</span>Validation finale</h3>
				<p>Une fois cette validation effectuée, le site est mis en ligne sur les serveurs et est désormais accessible partout dans le monde !</p>
			</div>
		</div>
	</div>
	<div class="container">
		<h2>Pourquoi faire appel à une agence Web ?</h2>
		<p>Nous avons chacun notre métier et le nôtre est de vous faire gagner du temps et de la crédibilité auprès de vos clients. La sécurité d'un site est également très importante afin de préserver vos données confidentielles ainsi que l'intégrité de votre site internet.</p>
		<p>Un site réalisé avec un outil de création de site internet rapide (Wordpress, WIX, Prestashop...) est généralement plus long à charger, car il n'est pas optimisé et comprend de nombreuses fonctionnalités qui ne sont pas utilisés, mais ralentissent tout de même le site. C'est pour cela que nous développons nous-même nos sites internet afin de n'intégrer uniquement ce qui est utile au projet et ainsi favoriser une meilleure rapidité du site.</p>
		Vous êtes convaincu ?<a href="#choix-offre" class="btn btn-secondary">Choisir une offre</a>
	</div>
</main>
@endsection
