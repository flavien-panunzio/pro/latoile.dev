@extends('layout.main')
@section('title',"Un abonnement \"Tout-compris\"")
@section('description',"Vous préférez que l'on s'occupe de tout ? Venez découvrir nos offres de création de site internet sous forme d'abonnement mensuel tout compris.")
@section('header')
<h1>"L'abonnement"</h1>
<h2>All-in-one</h2>
<p>Des frais mensuels fixe comprenant tout le nécessaire pour le bon fonctionnement de votre site internet</p>
@endsection
@section('includes')
<link rel="stylesheet" href="{{ asset('css/votre-projet.css') }}">
@endsection

@section('content')
<main class="abonnement">
	<div class="container">
		<h2>LE SITE WEB <b>CLÉS EN MAIN</b></h2>
		<p>Cette offre vous permet d'avoir un tarif mensuel <b>fixe et tout compris</b>. Notre offre est composée d'un premier montant pour la mise en ligne initiale de votre site puis d'<b>un abonnement mensuel</b>.</p>
	</div>
	<div class="offres">
		<h2>Nos offres</h2>
		<div class="row">
			<div  class="col-md-6 flex-center">
				<i class="fas fa-3x fa-address-card"></i>
				<h3>Portfolio</h3>
				<h4>Book en ligne</h4>
				<p>Nous vous proposons un site permettant d'exposer simplement votre travail sous la forme d'une galerie d'image.</p>
				<p><b>Tarif :</b> 300€ + 30€/mois HT</p>
			</div>
			<div  class="col-md-6 flex-center">
				<i class="fas fa-3x fa-store"></i>
				<h3>Site vitrine</h3>
				<h4>Présentation de l'activité</h4>
				<p>Un site vitrine de 5 pages (accueil, produits, à-propos, contact, mentions légales)</p>
				<p><b>Tarif :</b> 400€ + 40€/mois HT</p>
			</div>
			<div  class="col-md-6 flex-center">
				<i class="fas fa-3x fa-calendar-alt"></i>
				<h3>Site de réservation</h3>
				<h4>(Hôtel, Chambre d'hôte, Camping...)</h4>
				<p>Système de réservation avec paiement en ligne de vos clients afin de vous faciliter la gestion des réservations.</p>
				<p><b>Tarif :</b> 1 000 à 2 000€ + 100 à 200€/mois HT</p>
			</div>
			<div  class="col-md-6 flex-center">
				<i class="fas fa-3x fa-shopping-cart"></i>
				<h3>E-Commerce</h3>
				<h4>Site de vente en ligne</h4>
				<p>Site de vente en ligne complet avec la gestion des stocks, nouveaux produits, paiement en ligne, comptes clients, codes promos, commentaires clients...</p>
				<p><b>Tarif :</b> 3 000 à 5 000€ + 300 à 500€/mois HT</p>
			</div>
		</div>
		<h3>Vous êtes intéressés ?</h3>
		<a href="{{ route('contact') }}" class="btn btn-danger">Contactez-nous</a>
	</div>
	<div class="row details">
		<div class="col-12">
			<h2>L'offre en détails</h2>
			<h3>Toutes nos offres comprennent :</h3>
		</div>
		<div class="col-md-6">
			<table class="table table-hover">
				<tbody>
					<tr>
						<td>Frais d'hébergement et de nom de domaine</td>
					</tr>
					<tr>
						<td>Maintenance et mise à jour du site internet</td>
					</tr>
					<tr>
						<td>E-mail : mise en place et la configuration d'une (ou plusieurs) adresse e-mail (max : 5)</td>
					</tr>
					<tr>
						<td>Une formation sur l'utilisation de votre nouvel outil de travail</td>
					</tr>
					<tr>
						<td>Un service client réactif à votre écoute</td>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-6">
			<table class="table table-hover">
				<tbody>
					<tr>
						<td>Intégration de votre charte graphique (couleurs, logos, typographies...)</td>
					</tr>
					<tr>
						<td>Référencement optimisé et indexation du site (SEO Google et Bing)</td>
					</tr>
					<tr>
						<td>Statistiques : Outils de suivi d'activités du site (Google Analytics, Search Console...)</td>
					</tr>
					<tr>
						<td>Sauvegarde journalière des données de votre site</td>
					</tr>
					<tr>
						<td>La rédaction de vos mentions légales</td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</main>
@endsection
