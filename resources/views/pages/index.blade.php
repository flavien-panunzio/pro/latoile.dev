@extends('layout.main')
@section('description',"Vous cherchez votre nouvelle agence web ? Nous nous sommes spécialisés dans la création de site internet pour les PME, car nous sommes convaincus qu'une présence en ligne de qualité est la clé d'un business model fructueux. Venez découvrir toutes nos offres, nous sommes persuadés que vous trouverez celle qui correspond à votre projet Web !")
@section('includes')
<link rel="stylesheet" href="{{ asset('css/index.css') }}">
@endsection

@section('content')
<main class="index">
	<div class="presentation row">
		<div class="col-md-6">
			<img class="img" src="{{ asset('images/realisations/mockup.jpg') }}" alt="Maquette design de site e-commerce de vêtements crée par La Toile.dev">
		</div>
		<div class="col-md-6 flex-center">
			<h2>Que faisons-nous ?</h2>
			<p>Nous nous sommes spécialisés dans la création de site internet pour les PME, car nous sommes convaincus qu'une présence en ligne de qualité est la clé d'un business model fructueux.</p>
			<p>Nous vous proposons tout type d'offres allant du simple site vitrine pour présenter votre activité à un site de vente en ligne (e-commerce) complet et personnalisable.</p>
			<p>Venez découvrir toutes nos offres, nous sommes persuadés que vous trouverez celle qui vous correspond !</p>
			<a href="{{ route('votre-projet') }}" class="btn btn-secondary">Voir nos offres</a>
		</div>
	</div>
	<div class="stats">
		<h2>Quelques chiffres</h2>
		<div class="row">
			<div class="col-lg-3 col-md-6">
				<span class="typo">32%</span>
				d'augmentation moyen du nombre de ventes avec un site e-commerce
			</div>
			<div class="col-lg-3 col-md-6">
				<span class="typo">80%</span>
				des internautes font une recherche en ligne avant d’acheter
			</div>
			<div class="col-lg-3 col-md-6">
				<span class="typo">58%</span>
				des Français effectuent régulièrement des achats en ligne
			</div>
			<div class="col-lg-3 col-md-6">
				<span class="typo">67%</span>
				<div>des internautes naviguent sur internet <b>uniquement</b> sur téléphone</div>
			</div>
		</div>
		<cite><small>source : <a href="https://www.fevad.com/les-chiffres-cles-cartographie-du-e-commerce-en-2019/">FEVAD 2019</a></small></cite>
	</div>
	<div class="specs">
		<h2>Chaque site que nous réalisons est :</h2>
		<div class="grid">
			<div class="carre">
				<div class="flex-center">
					<h3><span class="typo">01.</span> Made in Creuse</h3>
					<p>Nous sommes situés sur Aubusson en Creuse <i>(Limousin, Nouvelle-Aquitaine)</i> afin d'être <b>au plus proche de vous</b> ce qui facilite les diverses rencontres que nous pourrions avoir lors de votre projet.</p>
				</div>
			</div>
			<div class="carre">
				<div class="flex-center">
					<h3><span class="typo">02.</span> Sécurisé</h3>
					<p>Nous effectuons quotidiennement de la veille afin de s'adapter aux <b>nouvelles normes de sécurité</b>. Nous réalisons nos sites internet avec le <i>framework</i> "Laravel" ce qui nous permet de sécuriser vos données.</p>
				</div>
			</div>
			<div class="carre">
				<div class="flex-center">
					<h3><span class="typo">03.</span> Personnalisé</h3>
					<p>Chaque site internet <i>"sur-mesure"</i> est unique et ne se base pas sur un modèle existant. Vous avez la possibilité de nous fournir une maquette de <b>ce que vous souhaitez</b> ou tout simplement venir avec vos idées afin que <b>nous concrétisons vos besoins.</b></p>
				</div>
			</div>
			<div class="carre">
				<div class="flex-center">
					<h3><span class="typo">04.</span> Responsive</h3>
					<p>Une personne sur cinq consulte internet sur un mobile, il va donc de soit qu'il est maintenant devenu indispensable d'avoir une <b>version adaptée aux mobiles</b> de son site internet.</p>
				</div>
			</div>
			<div class="carre">
				<div class="flex-center">
					<h3><span class="typo">05.</span> Optimisé</h3>
					<p>Chaque site internet est pensé pour être compatible avec les normes Google en terme de <b>référencement (SEO)</b>. Nous optimisons également la <b>vitesse de chargement</b> du site afin de fluidifier l'expérience utilisateur de vos clients.</p>
				</div>
			</div>
			<div class="carre">
				<div class="flex-center">
					<h3><span class="typo">06.</span> Suivi</h3>
					<p>Nous sommes <b>votre partenaire</b> tout au long de votre projet web, nous vous conseillons, réalisons et concrétisons vos souhaits ! Pour chaque site internet créé, une formation sur l'utilisation de ce dernier sera effectuée afin que vous puissiez utiliser au mieux vos outils.</p>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection
