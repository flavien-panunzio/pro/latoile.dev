@extends('layout.main')
@section('description',"Un projet ? Une idée ? Une question ? Nous sommes toujours prêts à vous accueillir et à en discuter ! Remplissez le formulaire pour nous contacter !")
@section('header')
	<h1>Contactez-nous</h1>
	<h2>Thé ou Café ?</h2>
	<p>Nous sommes à votre écoute pour toute demandes. N'hésitez pas à venir nous rencontrer pour nous parler de votre projet</p>
@endsection

@section('includes')
	<script src="{{ asset('js/contact.js') }}" defer></script>
@endsection

@section('content')
	<main class="container contact">
		<div class="row">
			<div class="col-12">
				<h2>Contactez <b>votre</b><br>Agence de Communication Creusoise</h2>
			</div>
			<div class="col-md-6 order-md-1 order-2">
				<iframe class="w-100" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d44387.18649788151!2d2.135076603968342!3d45.947303782435675!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47f9954f3a0e9bd1%3A0x998ca8099c1fc99c!2sLaToile.dev%20-%20Agence%20Web%20%C3%A0%20Aubusson%20(Creuse)%20de%20cr%C3%A9ation%20de%20site%20internet%20en%20Freelance!5e0!3m2!1sfr!2sfr!4v1598251351340!5m2!1sfr!2sfr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
			</div>
			<div class="col-md-6 order-md-2 order-1" id="form">
				@if ($message = Session::get('success'))
				<div class="alert alert-success alert-block text-center">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<span>{{ $message }}</span>
				</div>
				@endif

				@if ($message = Session::get('error'))
				<div class="alert alert-danger alert-block text-center">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<span>{!! $message !!}</span>
				</div>
				@endif

				@error('g-recaptcha-response')
				<div class="alert alert-danger alert-block text-center">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<span>{!! $errors->first('g-recaptcha-response') !!}</span>
				</div>
				@enderror
				{!!Form::open()->post()->route('mail')!!}
				<div class="form-row">
					<div class="col-sm-6">
						{!!Form::text('nom', 'Nom')!!}
					</div>
					<div class="col-sm-6">
						{!!Form::text('prenom', 'Prénom')!!}
					</div>
				</div>
				<div class="form-row">
					<div class="col-sm-6">
						{!!Form::text('email', 'E-mail')!!}
					</div>
					<div class="col-sm-6">
						{!!Form::text('tel', 'Téléphone')!!}
					</div>
				</div>
				{!!Form::textarea('message', 'Message')!!}
				{!!Form::submit('Envoyer le message')->color("secondary")!!}
				<input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response" required>
				{!!Form::close()!!}
			</div>
		</div>
	</main>
@endsection
