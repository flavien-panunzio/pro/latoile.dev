@component('mail::message')
# Formulaire de Contact

Une personne vous a contacté via le formulaire de contact présent sur votre site internet, voici ses informations et son message : <br>
***

**Nom :** {{ $data["nom"] }} <br>
**Prénom :** {{ $data["prenom"] }} <br>
**E-mail** : [{{ $data["email"] }}](mailto:{{ $data["email"] }}) <br>
**Téléphone :** [{{ $data["tel"] }}](tel:{{ $data["tel"] }}) <br>

**Message :** <br>
{{ $data["message"] }}

<br><br>

@component('mail::panel')
## Informations utilisateur :
**Adresse IP** : {{ $data["ip"] }} <br>
**Système d'exploitation** : {{ $data['OS'] }}<br>
**Navigateur** : {{ $data['navigateur'] }}
@endcomponent

@endcomponent
