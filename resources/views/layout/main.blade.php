<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<!-- LARAVEL -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="landing" content="{{ route('landing') }}">

	<!-- META OpenGraph -->
	<meta property="og:title" content="@if($__env->yieldContent('title')) @yield('title') | {{config('app.name')}} @else {{config('app.name')}} @endif">
	<meta property="og:site_name" content="LaToile.dev">
	<meta property="og:locale" content="fr_FR">
	<meta property="og:url" content="{{ url()->current() }}">
	<meta property="og:description" content="@yield('description')">
	<meta property="og:type" content="website">
	<meta property="og:image" content="{{ asset('images/opengraph.jpg') }}">
	<meta property="og:image:alt" content="Image de présentation de LaToile.dev">
	<meta property="fb:app_id" content="2546024529041401">

	<title>@if($__env->yieldContent('title')) @yield('title') | {{config('app.name')}} @else {{config('app.name')}} @endif</title>
	<meta name="description" content="@yield('description')">
	<meta name="keywords" content="site, web, internet, développeur, agence, création, La Toile Dev, LaToile.dev, La Toile, Aubusson">
	<meta name="author" content="Panunzio Flavien">
	<meta name="copyright" content="©{{ now()->year }} LaToile.dev">
	<link rel="canonical" href="{{ url()->current() }}">
	<meta name="robots" content="@yield('index')">

	<link rel="icon" href="{{ asset('images/favicon.png') }}" type="image/png">
	<meta name="theme-color" content="#182D23">

	<!-- FONT -->
	<link rel='preconnect' href='https://fonts.gstatic.com' crossorigin>
	<link rel="preload" as="style" onload="this.rel='stylesheet'" href="https://fonts.googleapis.com/css?family=Dosis:200,300,400,500,600,700,800&display=swap" type="text/css" media="all">
	<link rel="preload" as="style" onload="this.rel='stylesheet'" href="https://fonts.googleapis.com/css?family=Press+Start+2P&display=swap" type="text/css" media="all">
	<link rel="preload" as="style" onload="this.rel='stylesheet'" href="{{ asset('css/fontawesome.css') }}" type="text/css" media="all">

	<!-- CSS -->
	<link rel="preload" href="{{ mix('css/app.css') }}" as="style" onload="this.onload=null;this.rel='stylesheet'">
	<noscript><link rel="stylesheet" href="{{ mix('css/app.css') }}"></noscript>

	<!-- Scripts -->
	<script src="{{ asset('js/app.js') }}" defer></script>
	@yield('includes')

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-133505818-2"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-133505818-2');
	</script>
</head>
<body>
	@if (Route::currentRouteName()=='landing')
		<header class="flex-center landing" id="index" role="banner">
			<div class="flex-center video no-padding">
				<video class="img" poster="{{ asset('images/preview-video.jpg') }}" id="bgvid" playsinline autoplay muted loop><source src="{{ asset('images/presentation.webm') }}" type="video/webm"><source src="{{ asset('images/presentation.mp4') }}" type="video/mp4"></video>
			</div>
			<div class="flex-center">
				<div class="line"></div>
				<div class="logo">
					<img src="{{ asset('images/logo.svg') }}" alt="Logo de La Toile dev Aubusson Creuse">
				</div>
				<h1>Agence Web Creusoise</h1>
				<h2 class="d-none d-md-block">Création de sites internet à Aubusson</h2>
				<p>Spécialisés dans la création de sites internet personnalisés, nous prenons en charge votre projet de A à Z et nous vous accompagnons tout au long de votre projet.</p>
				<p class="d-none d-md-block no-padding">Créer un site internet personnalisé n'a jamais été aussi simple !</p>
				<div class="line"></div>
				<a role="button" class="btn btn-primary" href="{{ route('votre-projet') }}">Trouvez votre offre</a>
			</div>
		</header>
	@else
		<header class="flex-center default" style="background-image: url(@yield('background', asset('images/background.jpg')))");" role="banner">
			<div class="flex-center">
				<div class="logo">
					@if(Route::currentRouteName()=='print')
					<img src="{{ asset('images/logo_simple_sombre.svg') }}" alt="Logo agence de comm La Toile dev Aubusson Creuse">
					@else
					<img src="{{ asset('images/logo_simple.svg') }}" alt="Logo de La Toile dev Aubusson Creuse">
					@endif
				</div>
				@yield('header')
			</div>
		</header>
	@endif
	<nav class="navbar sticky-top navbar-expand-lg @if(Route::currentRouteName()=='print') bg-black navbar-dark @else navbar-light @endif" role="navigation">
		<a class="navbar-brand" href="{{ route('landing') }}">
			@if(Route::currentRouteName()=='print')
				<img src="{{ asset('images/logo.svg') }}" alt="Logo agence de comm La Toile dev Aubusson Creuse">
			@else
				<img src="{{ asset('images/logo_sombre.svg') }}" alt="Logo de La Toile dev">
			@endif
		</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
			<span class="burger"></span>
		</button>
		<div class="collapse navbar-collapse justify-content-center" id="navbar">
			<ul class="navbar-nav">
				<li class="nav-item">
					<a class="nav-link" href="{{ route('landing') }}">Accueil</a>
				</li>
				<li class="nav-item mon-dropdown">
					<div class="flex-center">
						<div class="dropdown-link">
							<a class="nav-link" href="{{ route('votre-projet') }}">Votre Projet Web</a>
							<a class="nav-link mon-dropdown-toggle">
								<i class="fas fa-caret-down"></i>
							</a>
						</div>
						<div class="dropdown-div flex-center">
							<a class="nav-link" href="{{ route('petit-budget') }}">"Le Petit Budget"</a>
							<a class="nav-link" href="{{ route('abonnement') }}">"L'abonnement"</a>
							<a class="nav-link" href="{{ route('sur-mesure') }}">"Le sur-mesure"</a>
						</div>
					</div>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('print') }}">Communication</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('realisations') }}">Nos réalisations</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('agence') }}">L'agence</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('contact') }}">Contact</a>
				</li>
			</ul>
		</div>
	</nav>

	<div class="wrapper">
		@yield('content')
		<div class="call-to-action">
			@if (url(request()->route()->getPrefix())==route('votre-projet'))
			<div class="flex-center">
				<h2>Vous avez un projet <span class="typo">?</span></h2>
				<h3>Discutons-en !</h3>
				<a class="btn btn-outline-danger" href="{{ route('contact') }}">Contactez votre agence Web</a>
			</div>
			@elseif(Route::currentRouteName()=='agence' || Route::currentRouteName()=='print')
			<div class="flex-center">
				<h2>Envie de nous rencontrer <span class="typo">?</span></h2>
				<p>Venez nous parler de vos projets !</p>
				<a class="btn @if(Route::currentRouteName()=='print') btn-outline-warning @else btn-outline-danger @endif" href="{{ route('contact') }}">Thé ou café ?</a>
			</div>
			@elseif(Route::currentRouteName()!='contact')
			<div class="flex-center">
				<h2>Vous êtes intéressés <span class="typo">?</span></h2>
				<p>Nous vous proposons plusieurs types d'offres, vous trouverez forcément celle qui correspond à votre projet.</p>
				<a class="btn btn-outline-danger" href="{{ route('votre-projet') }}">Voir nos offres</a>
			</div>
			@endif
		</div>
	</div>
	<footer id="particles-js">
		<div class="flex-center">
			<h2><a href="{{ route('contact') }}">Contactez-nous</a></h2>
			<div class="ligne">
				<i class="fas fa-address-card fa-2x"></i>
				<a href="{{ route('agence') }}">
					@if(Route::currentRouteName()=='print')
					<img src="{{ asset('images/logo_texte_blanc.svg') }}" alt="Logo de La Toile dev">
					@else
					<img src="{{ asset('images/logo_texte.svg') }}" alt="Logo de La Toile dev">
					@endif
				</a>
			</div>
			<div class="ligne">
				<i class="fab fa-facebook-square fa-2x"></i>
				<a href="https://www.facebook.com/LaToile.dev">Suivez-nous sur Facebook</a>
			</div>
			<div class="ligne">
				<i class="fa fa-at fa-2x"></i>
				<a href="mailto:contact@latoile.dev">contact@latoile.dev</a>
			</div>
			<div class="ligne">
				<i class="fas fa-phone fa-2x"></i>
				<a href="tel:0678871814">06.78.87.18.14</a>
			</div>
			<div class="ligne">
				<span class="mr-4">©{{ now()->year }} LaToile.dev</span><a href="{{ route('mentions') }}">Mentions Légales</a>
			</div>
			<div class="business">
				<a class="btn @if(Route::currentRouteName()=='print') btn-outline-warning @else btn-outline-danger @endif" href="https://g.page/latoile-dev/review?gm">Vous aimez notre travail ? <br> Dites-le nous !</a>
			</div>
			<div class="ligne marketplace">
				<a class="mr-2" href="https://www.codeur.com/-latoile-dev">
					<img src="{{ asset('images/codeur.svg') }}" alt="Logo Codeur.com" title="Codeur.com" height="auto" width="50px">
				</a>
				<a class="mr-2" href="https://www.malt.fr/profile/flavienpanunzio">
					<img src="{{ asset('images/logo_malt.svg') }}" alt="Logo Malt" title="Malt" height="auto" width="50px">
				</a>
				<a class="mr-2" href="https://www.linkedin.com/company/latoile-dev">
					<img src="{{ asset('images/linkedin.svg') }}" alt="Logo Linkedin" title="Linkedin" height="auto" width="50px">
				</a>
				<a href="https://plateforme.freelance.com/freelance/Flavien-6e26197b-29b7-4e14-93f2-1679fd85d50c">
					<img src="{{ asset('images/freelance.svg') }}" alt="Logo Freelance.com" title="Freelance.com" height="auto" width="50px">
				</a>
			</div>
		</div>
	</footer>
	@yield('modal')
</body>
</html>
