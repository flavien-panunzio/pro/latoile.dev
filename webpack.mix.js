const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.disableNotifications();

mix.js('resources/js/app.js', 'public/js')
.js('resources/js/print.js', 'public/js')
.js('resources/js/agence.js', 'public/js')
.js('resources/js/realisations.js', 'public/js')
.js('resources/js/contact.js', 'public/js')

.sass('resources/sass/app.scss', 'public/css')
.sass('resources/sass/fontawesome.scss', 'public/css')

.sass('resources/sass/pages/agence.scss', 'public/css')
.sass('resources/sass/pages/index.scss', 'public/css')
.sass('resources/sass/pages/realisations.scss', 'public/css')
.sass('resources/sass/pages/print.scss', 'public/css')
.sass('resources/sass/pages/votre-projet.scss', 'public/css').sourceMaps();


mix.copy('resources/js/hover-dir.js', 'public/js/hover-dir.js');
