<?php

namespace App\Http\Controllers;

use Log;
use App\Mail\CustomMail;
use Illuminate\Support\Facades\Mail;

class MailController extends Controller
{
	// Comment utiliser la fonction : MailController::sendMail($user->email, "Mail de Contact", 'emails.contact', $request->all(), $request->ip(), "flavien.panunzio@gmail.com");
	public static function sendMail($to, $subject, $view, $data, $ip = null, $copy = null) {
		if ($ip!=null) $data['ip'] = $ip;
		$data['OS'] = MailController::getOS();
		$data['navigateur'] = $_SERVER['HTTP_SEC_CH_UA'];
		try{
			return Mail::to($to)->bcc($copy)->send(new CustomMail($subject, $view, $data));
		}
		catch(\Exception $e){
			Log::channel('mail')->error("***** MailController ***** Envoi impossible $to : $subject ".$e->getMessage());
		}
	}

	public static function getOS() {
		$os_platform =   "";
		$os_array = [
			'/windows nt 11/i'      =>  'Windows 11',
			'/windows nt 10/i'      =>  'Windows 10',
			'/windows nt 6.3/i'     =>  'Windows 8.1',
			'/windows nt 6.2/i'     =>  'Windows 8',
			'/windows nt 6.1/i'     =>  'Windows 7',
			'/windows nt 6.0/i'     =>  'Windows Vista',
			'/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
			'/windows nt 5.1/i'     =>  'Windows XP',
			'/windows xp/i'         =>  'Windows XP',
			'/windows nt 5.0/i'     =>  'Windows 2000',
			'/windows me/i'         =>  'Windows ME',
			'/win98/i'              =>  'Windows 98',
			'/win95/i'              =>  'Windows 95',
			'/win16/i'              =>  'Windows 3.11',
			'/macintosh|mac os x/i' =>  'Mac OS X',
			'/mac_powerpc/i'        =>  'Mac OS 9',
			'/linux/i'              =>  'Linux',
			'/ubuntu/i'             =>  'Ubuntu',
			'/iphone/i'             =>  'iPhone',
			'/ipod/i'               =>  'iPod',
			'/ipad/i'               =>  'iPad',
			'/android/i'            =>  'Android',
			'/blackberry/i'         =>  'BlackBerry',
			'/webos/i'              =>  'Mobile'
		];

		foreach ($os_array as $regex => $value) {
			if (preg_match($regex, $_SERVER['HTTP_USER_AGENT'])) {
				$os_platform = $value;
			}
		}
		return $os_platform;
	}
}
