<?php

namespace App\Http\Controllers;

use App\Rules\Recaptcha;
use Illuminate\Http\Request;
use App\Http\Controllers\MailController;

class PostsController extends Controller{

	public function sendMail(Request $request){
		$request->validate([
			'nom' => 'required',
			'prenom' => 'required',
			'email' => 'required|email',
			'tel' => 'required',
			'message' => 'required',
			'g-recaptcha-response' => new Recaptcha
		]);
		MailController::sendMail(config('mail.from.address'), "Formulaire de contact LaToile.dev", 'emails.contact', $request->all(), $request->ip());
		return redirect()->back()->with('success', "Votre message a bien été envoyé, nous vous répondrons au plus vite.");
	}

}
