<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\InvokableRule;

class Recaptcha implements InvokableRule {

	public function __invoke($attribute, $value, $fail) {
		$captcha = json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=" . config('services.captcha.secret') . "&response={$value}"));

		if (!property_exists($captcha, "score") || ($captcha->success == false || $captcha->score < 0.5)) {
			$fail("Vous avez été détecté comme un robot, merci de réessayer.<br> Si le problème persiste, contactez-nous par <a href='mailto:" . config('mail.from.address') . "'>e-mail</a> directement : " . config('mail.from.address'));
		}
	}
}
