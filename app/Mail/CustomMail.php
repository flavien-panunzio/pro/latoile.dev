<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class CustomMail extends Mailable {
	use Queueable, SerializesModels;

	public $subject, $view, $data;

	public function __construct($subject, $view, $data){
		$this->subject = $subject;
		$this->view = $view;
		$this->data = $data;
	}

	public function build(){
		return $this
			->subject($this->subject." | ".config('app.name'))
			->markdown($this->view)
			->with(['data' => $this->data]);
	}
}
